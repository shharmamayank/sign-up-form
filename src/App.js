import React, { Component } from 'react'
import Form from './component/Form'

export class App extends Component {
  render() {
    return (
      <div>
        <Form />
      </div>
    )
  }
}

export default App
