import React, { Component } from 'react'
import validator from "validator";
import "./Form.css"

export class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FirstName: "",
            LastName: "",
            Age: "",
            Gender: "",
            Role: "",
            Email: "",
            password: "",
            confirmPassword: "",
            isChecked: false,
            passed: false,
            errors: {},
        };
    }

    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const errors = {};

        if (validator.isEmpty(this.state.FirstName)) {
            errors.FirstName = "please fill the blank";
        } else if (!validator.isAlpha(this.state.FirstName)) {
            errors.FirstName = "First Name should only have alphabets";
        }

        if (validator.isEmpty(this.state.LastName)) {
            errors.LastName = "please fill the blank";
        } else if (!validator.isAlpha(this.state.LastName)) {
            errors.LastName = "Last Name should only have alphabets";
        }
        if (!validator.isInt(this.state.Age)) {
            errors.Age = "Enter Valid Number";
        } else if (this.state.Age > 0) {
            errors.Age = "Please Enter Age ";
        }
        if (!validator.isEmail(this.state.Email)) {
            errors.Email = "Invalid Email address";
        }
        if (!validator.isLength(password, { min: 8 })) {
            errors.password = "Password must be at least 8 characters long.";
        }

        if (password !== confirmPassword) {
            errors.confirmPassword = "Password does not match.";
        }

        if (Object.keys(errors).length > 0) {
            this.setState({ errors });
            return;
        }
        console.log("Form submitted!");
        this.setState({ passed: true }); if (Object.keys(errors).length > 0) {
            this.setState({ errors });
            return;
        }
        console.log("Form submitted!");
        this.setState({ passed: true });


        


    };

    render() {
        const {
            FirstName,
            LastName,
            password,
            errors,
            Gender,
            Age,
            Role,
            Email,
            confirmPassword,
            isChecked,
            passed,
        } = this.state;


        return (
            <>
                {passed}
                <div className='d-flex justify-content-center text-info bg-dark'><h1>Sign-Up-Form</h1>
                </div>

                <form onSubmit={this.handleSubmit} class="border mx-3 d-flex flex-column align-items-center border border-secondary p-3 mb-2 bg-secondary text-white">
                    <div className='d-flex flex-column justify-content-center '>
                        <span className='mx-5'>Name:</span>

                        <div class="row d-flex justify-content-center">
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="FirstName"
                                    value={FirstName}
                                    onChange={this.handleInputChange}
                                    placeholder="First name" />
                                {errors.FirstName && (
                                    <p className="text-danger">{errors.FirstName}</p>
                                )}

                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="LastName"
                                    value={LastName}
                                    onChange={this.handleInputChange}
                                    placeholder="Last name" />
                                {errors.LastName && (
                                    <p className="text-danger">{errors.LastName}</p>
                                )}

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 d-flex justify-content-around">
                        <div className='col-md-7 mt-2'>
                            <label for="validationCustom04" class="form-label">Gender</label>
                            <select class="form-select" name="Gender"
                                value={Gender}
                                onChange={this.handleInputChange}
                                required>
                                <option selected disabled value="">Choose...</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select>
                            <div class="invalid-feedback">
                                Please select a valid state.

                            </div>
                        </div>

                        <div class="col-md-3 mt-2">
                            <label for="validationCustom04" class="form-label">Age</label>
                            <input type="text" name="Age"
                                value={Age}
                                onChange={this.handleInputChange}
                                class="form-control" placeholder="Age" />
                            {errors.Age && <p className="text-danger">{errors.Age}</p>}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="sr-only mt-2" for="inlineFormInputGroup">Email:</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">@</div>
                            </div>
                            <input type="text" class="form-control" name="Email"
                                value={Email}
                                onChange={this.handleInputChange}
                                placeholder="Username" />
                        </div>
                    </div>
                    <div class="col-md-3 d-flex flex-column justify-content-around">

                        <label for="validationCustom04" class="form-label">Role</label>
                        <select class="form-select" id="validationCustom04" name="Role"
                            value={Role}
                            onChange={this.handleInputChange}
                            required>

                            <option selected disabled value="">Choose...</option>
                            <option>Developer</option>
                            <option>Senior Developer</option>
                            <option>Lead Engineer</option>
                            <option>CTO</option>
                        </select>
                        <div class="invalid-feedback">
                            Please select a valid state.
                        </div>
                    </div>
                    <div class="form-group col-md-4 mt-2">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password"
                            value={password}
                            onChange={this.handleInputChange}
                            id="exampleInputPassword1" placeholder="Password" />
                        {errors.password && (
                            <p className="text-danger">{errors.password}</p>
                        )}

                    </div>
                    <div class="form-group col-md-4 mt-2">
                        <label for="exampleInputPassword1">Confirm Password</label>
                        <input type="password" class="form-control" name="confirmPassword"
                            value={confirmPassword}
                            onChange={this.handleInputChange}
                            placeholder="Password" />
                        {errors.confirmPassword && (
                            <p className="text-danger">{errors.confirmPassword}</p>
                        )}

                    </div>
                    <div class="form-check mt-4">
                        <input type="checkbox" name="isChecked"
                            value={true}
                            onChange={this.handleInputChange}
                            class="form-check-input" id="exampleCheck1" />
                        <label class="form-check-label" id='condition' for="exampleCheck1">Agree to term and condition</label>
                    </div>
                    <button type="submit" class="btn btn-dark mt-3">Submit</button>

                </form>
                {/*<h1 className={this.state.success} > Sign in Successful </h1> */}

            </>
        )
    }
}

export default Form
